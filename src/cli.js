#!/usr/bin/env node

const getopts = require("getopts");
const isNil = require("lodash/isNil");
const isArray = require("lodash/isArray");
const isString = require("lodash/isString");
const compact = require("lodash/compact");
const uniq = require("lodash/uniq");
const searchFilenames = require("./index");

const opts = getopts(process.argv, {
  alias: {
    concurrency: "c",
    "root-path": "root",
    include: "i",
    "include-glob": "ig",
    "include-regexp": "ir",
    "include-function": "ifn",
    exclude: "e",
    "exclude-glob": "eg",
    "exclude-regexp": "er",
    "exclude-function": "efn",
    "exclude-absolute": "ea",
    "follow-symlinks": "fs",
    "stat-type": "st",
    "stop-on-error": "se"
  }
});

let searcher;
const format = opts.format || "text";
let exitCode = 0;

const searchOpts = {
  concurrency: opts["concurrency"],
  rootPath: opts["root-path"] || process.cwd(),
  includes: opts.include,
  includeGlobs: opts["include-glob"],
  includeRegExp: opts["include-regexp"],
  includeFunctions: opts["include-function"],
  excludes: opts.exclude,
  excludeGlobs: opts["exclude-glob"],
  excludeRegExp: opts["exclude-regexp"],
  excludeFunctions: opts["exclude-function"],
  excludeAbsolutes: opts["exclude-absolute"],
  followSymlinks: opts["follow-symlinks"],
  statTypes: opts["stat-type"],
  stopOnError: opts["stop-on-error"]
};

const stringToArrayOptions = [
  "includes",
  "includeGlobs",
  "includeRegExp",
  "includeFunctions",
  "excludes",
  "excludeGlobs",
  "excludeRegExp",
  "excludeFunctions",
  "excludeAbsolutes",
  "statTypes"
];

// convert string options into arrays
stringToArrayOptions.forEach(function (name) {
  if (isString(searchOpts[name]) === true) {
    searchOpts[name] = [searchOpts[name]];
  }

  // cut down duplicates or blanks, if any
  searchOpts[name] = compact(uniq(searchOpts[name]));
});

function reportError(err) {
  exitCode = searchOpts.stopOnError ? 1 : 10;

  const { message, stack } = err;

  if (format === "json") {
    if (
      isNil(err.validation) === false &&
      isArray(err.validation.errors) === true
    ) {
      err.validation.errors.forEach(function (item) {
        console.error(JSON.stringify(item));
      });
    } else {
      console.error({ error: { message, stack } });
    }
  } else {
    console.error(message);
    if (
      isNil(err.validation) === false &&
      isArray(err.validation.errors) === true
    ) {
      err.validation.errors.forEach(function (item) {
        console.error(item.dataPath, item.message);
      });
    }
  }

  if (searcher !== undefined && searchOpts.stopOnError) {
    searcher.stop();
  }
}

try {
  searcher = searchFilenames(searchOpts);
} catch (err) {
  reportError(err);
  process.exit(1);
}

searcher.on("error", reportError);

function writeEmitted(res) {
  let absolutePath;

  if (isString(res) === true) {
    absolutePath = res;
  } else {
    absolutePath = res.absolutePath;
  }

  if (format === "json") {
    console.log(JSON.stringify(res));
  } else {
    console.log(absolutePath);
  }
}

searcher.on("any", writeEmitted);
// searcher.on("block-device", res => writeEmitted("block-device", res));
// searcher.on("character-device", res => writeEmitted("character-device", res));
// searcher.on("fifo", res => writeEmitted("fifo", res));
// searcher.on("socket", res => writeEmitted("socket", res));
// searcher.on("symlink", res => writeEmitted("symlink", res));
// searcher.on("directory", res => writeEmitted("directory", res));
// searcher.on("file", res => writeEmitted("file", res));

searcher.on("end", function () {
  process.exit(exitCode);
});

searcher.start();
