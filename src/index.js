const fs = require("fs");
const path = require("path");
const { EventEmitter } = require("events");
const queue = require("async/queue");
const micromatch = require("micromatch");
const isNil = require("lodash/isNil");
const isEmpty = require("lodash/isEmpty");
const isObject = require("lodash/isObject");
const isRegExp = require("lodash/isRegExp");
const isFunction = require("lodash/isFunction");
const compact = require("lodash/compact");
const uniq = require("lodash/uniq");
const optsValidator = require("./opts-validator");

const statTypeShortcutMap = {
  bd: "block-device",
  cd: "character-device",
  d: "directory",
  f: "file",
  l: "symlink",
  s: "socket"
};

/**
 * Create a file searcher which emits filenames that match
 * @method searchFilenames
 * @param {object} opts see ./opts-schema.json for the options
 * @returns {object} emitter
 */
function searchFilenames(opts) {
  const areOptionsValid = optsValidator(opts);

  if (areOptionsValid === false) {
    const { errors } = optsValidator;
    const err = new Error("The search options were invalid.");
    err.validation = { errors };
    throw err;
  }

  const emitter = new EventEmitter();
  let listStatQueue;
  let allIncludesBlank = false;

  /**
   * It's called when the queue drains of all tasks.
   * @method end
   * @param {object} [err]
   */
  function end(err) {
    if (isNil(err) === false) {
      emitter.emit("error", err);

      if (opts.stopOnError && isObject(listStatQueue)) {
        listStatQueue.kill();
      }
    }

    emitter.emit("end");
  }

  /**
   * Initialize include and exclude methods.
   * @method startList
   * @private
   */
  function startList() {
    const {
      concurrency,
      rootPath,
      includes,
      includeGlobs,
      excludes,
      excludeGlobs,
      excludeAbsolutes,
      followSymlinks
    } = opts;

    let {
      statTypes,
      includeRegExp,
      includeFunctions,
      excludeRegExp,
      excludeFunctions
    } = opts;

    // convert stat type short names into the longer form
    statTypes = compact(
      uniq(statTypes.map(item => statTypeShortcutMap[item] || item))
    );

    const rootResolvedPath = path.resolve(rootPath);

    let includeGlobsMatcher;
    let excludeGlobsMatcher;

    if (includeGlobs.length > 0) {
      includeGlobsMatcher = micromatch.matcher(includeGlobs);
    }

    if (excludeGlobs.length > 0) {
      excludeGlobsMatcher = micromatch.matcher(excludeGlobs);
    }

    if (includeRegExp.length > 0) {
      includeRegExp = includeRegExp.map(function (item) {
        if (isRegExp(item) === false) {
          return new RegExp(item, "gi");
        }

        return item;
      });
    }

    if (excludeRegExp.length > 0) {
      excludeRegExp = excludeRegExp.map(function (item) {
        if (isRegExp(item) === false) {
          return new RegExp(item, "gi");
        }

        return item;
      });
    }

    allIncludesBlank =
      isEmpty(includes) &&
      isEmpty(includeGlobs) &&
      isEmpty(includeRegExp) &&
      isEmpty(includeFunctions);

    /**
     * Try to load the JS function or module for include and exclude.
     * @method loadModule
     * @param {string} modulePath
     * @returns {function}
     */
    function loadModule(modulePath) {
      if (isFunction(modulePath)) {
        return modulePath;
      }
      try {
        let mod = require(modulePath);
        if (
          isObject(mod) &&
          isFunction(mod) === false &&
          isFunction(mod.default)
        ) {
          mod = mod.default;
        }
      } catch (err) {
        emitter.emit("error", err);
        if (opts.stopOnError) {
          return emitter.stop();
        }
      }
    }

    includeFunctions = includeFunctions.map(loadModule);
    excludeFunctions = excludeFunctions.map(loadModule);

    /**
     * Test if an absolute path is matched for inclusion.
     * @method isPathExcluded
     * @private
     * @param {string} absolutePath
     * @returns {boolean}
     */
    function isPathExcluded(absolutePath) {
      const basename = path.basename(absolutePath);

      // exclude by absolute path
      const exAbs = excludeAbsolutes.some(item =>
        absolutePath.startsWith(item)
      );

      if (exAbs) {
        return true;
      }

      // exclude by normal string
      const exString = excludes.some(
        item => absolutePath.includes(item) || basename.includes(item)
      );

      if (exString) {
        return true;
      }

      // exclude by glob / micromatch
      if (
        excludeGlobsMatcher !== undefined &&
        (excludeGlobsMatcher(absolutePath) || excludeGlobsMatcher(basename))
      ) {
        return true;
      }

      // exclude reg exp test
      if (excludeRegExp !== undefined && excludeRegExp.length > 0) {
        const exRegExp = excludeRegExp.some(
          item => item.test(absolutePath) || item.test(basename)
        );
        if (exRegExp) {
          return true;
        }
      }

      // exclude by custom node function
      if (excludeFunctions.length > 0) {
        const exFun = excludeFunctions.some(item => item(absolutePath));
        if (exFun) {
          return true;
        }
      }

      return false;
    }

    /**
     * Test if an absolute path is matched for exclusion.
     * @method isPathIncluded
     * @private
     * @param {string} absolutePath
     * @returns {boolean}
     */
    function isPathIncluded(absolutePath) {
      if (allIncludesBlank) {
        return true;
      }

      const basename = path.basename(absolutePath);

      // include by string
      const incString = includes.some(function (item) {
        return absolutePath.includes(item) || basename.includes(item);
      });

      if (incString) {
        return true;
      }

      // include by glob / miromatch
      if (
        includeGlobsMatcher !== undefined &&
        (includeGlobsMatcher(absolutePath) || includeGlobsMatcher(basename))
      ) {
        return true;
      }

      // include by regex test
      if (includeRegExp !== undefined && includeRegExp.length > 0) {
        const incRegExp = includeRegExp.some(
          item => item.test(absolutePath) || item.test(basename)
        );
        if (incRegExp) {
          return true;
        }
      }

      // include by custom node function
      if (includeFunctions.length > 0) {
        const incFun = includeFunctions.some(item => item(absolutePath));
        if (incFun) {
          return true;
        }
      }

      return false;
    }

    /**
     * Process a list of paths to check for more files.
     * @method processFileList
     * @private
     * @param {string} options.absolutePath
     * @param {string} options.filenameList
     */
    function processFileList({ absolutePath, filenameList }) {
      if (filenameList.length === 0) {
        return;
      }

      if (isPathExcluded(absolutePath)) {
        return;
      }

      filenameList
        .map(filename => ({
          operation: "stat",
          absolutePath: path.join(absolutePath, filename)
        }))
        .filter(item => isPathExcluded(item.absolutePath) === false)
        .forEach(item => listStatQueue.push(item));
    }

    /**
     * Determine if a file stat type is in our list specified by the user.
     * @method isStatTypeIncluded
     * @private
     * @param {string} typeName file stat type
     * @returns {boolean}
     */
    function isStatTypeIncluded(typeName) {
      return statTypes.length === 0 || statTypes.includes(typeName);
    }

    /**
     * When a stat and lstat come in it's sent here. Based on the file type
     * it will branch out to other operations.
     * @method processStat
     * @private
     * @param {object} options.lstat
     * @param {object} options.stat
     * @param {string} options.absolutePath
     */
    function processStat({ lstat, stat, absolutePath }) {
      const isSymbolicLink = lstat.isSymbolicLink();
      const isDirectory = stat.isDirectory();
      const isBlockDevice = stat.isBlockDevice();
      const isCharacterDevice = stat.isCharacterDevice();
      const isFIFO = stat.isFIFO();
      const isSocket = stat.isSocket();
      const isFile = stat.isFile();
      const operation = "list";

      const included = isPathIncluded(absolutePath);

      if (isDirectory) {
        if (isSymbolicLink && followSymlinks) {
          listStatQueue.push({ operation, absolutePath });
        } else if (isSymbolicLink === false) {
          listStatQueue.push({ operation, absolutePath });
        }

        if (included && isStatTypeIncluded("directory")) {
          emitter.emit("any", {
            statType: "directory",
            absolutePath,
            isSymbolicLink
          });
        } else if (
          included &&
          isSymbolicLink &&
          isStatTypeIncluded("symlink")
        ) {
          emitter.emit("any", {
            statType: "directory",
            absolutePath,
            isSymbolicLink
          });
        }

        if (included && isSymbolicLink && isStatTypeIncluded("symlink")) {
          emitter.emit("symlink", absolutePath);
        }

        if (included && isStatTypeIncluded("directory")) {
          emitter.emit("directory", absolutePath);
        }

        return;
      }

      if (included && isBlockDevice && isStatTypeIncluded("block-device")) {
        emitter.emit("any", { statType: "block-device", absolutePath });
        emitter.emit("block-device", absolutePath);
        return;
      }

      if (
        included &&
        isCharacterDevice &&
        isStatTypeIncluded("character-device")
      ) {
        emitter.emit("any", { statType: "chracter-device", absolutePath });
        emitter.emit("chracter-device", absolutePath);
        return;
      }

      if (included && isFIFO && isStatTypeIncluded("fifo")) {
        emitter.emit("any", { statType: "fifo", absolutePath });
        emitter.emit("fifo", absolutePath);
        return;
      }

      if (included && isSocket && isStatTypeIncluded("socket")) {
        emitter.emit("any", { statType: "socket", absolutePath });
        emitter.emit("socket", absolutePath);
        return;
      }

      if (
        included &&
        isFile &&
        isStatTypeIncluded("file") &&
        isDirectory === false
      ) {
        emitter.emit("any", { statType: "file", absolutePath });
        emitter.emit("file", absolutePath);
        return;
      }
    }

    /**
     * When tasks are pushed onto the queue they run through this.
     * @method worker
     * @private
     * @param {string} options.operation
     * @param {string} options.absolutePath
     * @param {function} done
     */
    function worker({ operation, absolutePath }, done) {
      if (isPathExcluded(absolutePath)) {
        return false;
      }

      if (operation === "list") {
        fs.readdir(absolutePath, function (err, filenameList) {
          if (isNil(err) === false) {
            return done(err);
          }

          processFileList({ absolutePath, filenameList });
          done();
        });
      } else if (operation === "stat") {
        fs.lstat(absolutePath, function (err, lstat) {
          if (isNil(err) === false) {
            return done(err);
          }

          fs.stat(absolutePath, function (err, stat) {
            if (isNil(err) === false) {
              if (lstat.isSymbolicLink() && err.code === "ENOENT") {
                /*

                this is when there is a symlink that exists but the place
                it points to doesn't exist

                */
                return done();
              }

              return done(err);
            }

            processStat({ lstat, stat, absolutePath });
            done();
          });
        });
      } else {
        throw new Error("unknown operation");
      }
    }

    listStatQueue = queue(worker, concurrency);
    listStatQueue.error(end);

    fs.exists(rootResolvedPath, function (exists) {
      if (exists === false) {
        const err = new Error(`rootPath doesn't exist: ${rootPath}`);
        return end(err);
      }

      const operation = "list";
      const absolutePath = rootResolvedPath;
      listStatQueue.push({ operation, absolutePath });
    });
  }

  /**
   * Start running the directory and file searching.
   * @method emitter.start
   */
  emitter.start = function () {
    startList();
    listStatQueue.drain(end);
  };

  /**
   * Stop the search.
   * @method emitter.stop
   */
  emitter.stop = function () {
    end();
  };

  return emitter;
}

module.exports = searchFilenames;
