# search-filenames

The module and CLI `search-filenames` searches the file system for any kind of path **fast**. It has options for include, exlude, and few others. The goal is to be simple, fast, and robust. It should be a reliable and bug-free module that can be used in other tools or by users who want to efficiently search their computer.

There are some similarities with `find` command but `search-filenames` is much simpler. You can see the section called \[#differences-from-find]\(Differences from find).

## Table of contents

+ [Install](#install)
+ [CLI](#cli)
+ [Stat types](#stat-types)
+ [JS API](#js-api)
+ [Include and exclude priority](#include-and-exclude-priority)
  + [Events](#events)
+ [Development](#development)
+ [Differences from find](#differences-from-find)
+ [Convenience](#convenience)
  + [Alias](#alias)
  + [zsh function](#zsh-function)
+ [Copying, license, and contributing](#copying-license-and-contributing)

## Install

For using the JS API:

`npm install search-filenames`

Using globally with CLI:

`npm install --global search-filenames`

## CLI

```sh
search-filenames --root-path ~ --exclude node_modules --exclude .git --include-glob '*.sublime-workspace' --concurrency=16
```

| Option               | alias  | one or many | Description                                                         |
| -------------------- | ------ | ----------- | ------------------------------------------------------------------- |
| `--root-path`        | `root` | one         | Where to begin searching                                            |
| `--include`          | `i`    | many        | The path includes the text exactly                                  |
| `--include-glob`     | `ig`   | many        | The path matches the glob pattern                                   |
| `--include-regexp`   | `ir`   | many        | The path matches the JavaScript `RegExp` pattern                    |
| `--include-function` | `ifn`  | many        | The file was matched by the function `true` output                  |
| `--exclude`          | `e`    | many        | The path does not include the text exactly                          |
| `--exclude-glob`     | `eg`   | many        | The path does not match the glob pattern                            |
| `--exclude-regexp`   | `er`   | many        | The path does not match the JavaScript `RegExp` pattern             |
| `--exclude-function` | `efn`  | many        | The file was excluded by the function `true` output                 |
| `--exclude-absolute` | `ea`   | many        | The path does not start with this absolute path. e.g. /home/bubba   |
| `--follow-symlinks`  | `fs`   | one         | Search inside where the symlink directory goes. Defaults to `false` |
| `--concurrency`      | `c`    | one         | How many I/O operations can it initiate at a time? Defaults to `8`  |
| `--stat-type`        | `st`   | many        | Only include these stat types in the output                         |
| `--stop-on-error`    | `se`   | one         | If there is any kind of file system error stop. e.g. EACCESS        |

## Stat types

Stat types correspond to what information we can get from the [node fs](https://nodejs.org/dist/latest/docs/api/fs.html#fs_class_fs_dirent) `fs.stat` and `fs.lstat` functions.

| Stat type        | Long form                      | Short form |
| ---------------- | ------------------------------ | ---------- |
| character-device | `--stat-type character-device` | `--st cd`  |
| block-device     | `--stat-type block-device`     | `--st bd`  |
| directory        | `--stat-type directory`        | `--st d`   |
| fifo             | `--stat-type fifo`             |            |
| file             | `--stat-type file`             | `--st f`   |
| symlink          | `--stat-type symlink`          | `--st l`   |
| socket           | `--stat-type socket`           | `--st s`   |

## JS API

+ `concurrency`, `number`, optional, default `8`
+ `rootPath`, `string`, required
+ `includes`, `string[]`, optional, default `[]`
+ `includeGlobs`, `string[]`, optional, default `[]` converted into [https://github.com/micromatch/micromatch](micromatch)
+ `includeRegExp`, `string[]`, required, default `[]` converted into JavaScript RegExp
+ `includeFunctions`, `string[]`, optional, default `[]` your node.js function or module path with `module.exports` or `export default`
+ `excludes`, `string[]`, optional, default `[]`
+ `excludeGlobs`, `string[]`, optional, default `[]` converted into [https://github.com/micromatch/micromatch](micromatch)
+ `excludeRegExp`, `array`, required, default `[]` converted into JavaScript RegExp
+ `excludeAbsolutes`, `string[]`, optional, default `[]`
+ `excludeFunctions`, `string[]`, optional, default `[]` your node.js function or module path with `module.exports` or `export default`
+ `followSymlinks`, `array`, required, default `[]`
+ `statTypes`, `array`, optional, default `[]` \[#stat-types]\(Stat types)

If there are no includes, and no stat types, and it's not excluded then it's included. (like doing `find .`)

```js
const searchFilenames = require("search-filenames");

const searchOpts = {
  rootPath: "/home/tcrowe/projects",
  includeGlobs: ["*.{jpg,png,gif}"],
  excludes: ["node_modules", ".git"]
};

const searcher = searchFilenames(searchOpts);

searcher.on("error", err => console.error("searcher error", err));
searcher.on("file", res => console.log("file", res));
searcher.start();
```

## Include and exclude priority

Includes and excludes are done in a specific order.

1. `excludeAbsolutes`
2. `includes` `excludes`
3. `includeGlobs` `excludeGlobs`
4. `includeRegExp` `excludeRegExp`
5. `includeFunctions` `excludeFunctions`

### Events

+ `any` ➡️ ({ statType:`string`, absolutePath:`string` }) any files matched the conditions
+ `block-device` ➡️ (absolutePath:`string`)
+ `character-device` ➡️ (absolutePath:`string`)
+ `fifo` ➡️ (absolutePath:`string`)
+ `socket` ➡️ (absolutePath:`string`)
+ `symlink` ➡️ (absolutePath:`string`)
+ `directory` ➡️ (absolutePath:`string`)
+ `file` ➡️ (absolutePath:`string`)
+ `error` ➡️ (`Error`)
+ `end`

## Development

The development tasks run under `npm run dev`

## Differences from find

+ *does not* and cannot execute any other command or delete
+ *does not* do query optimisation `-olevel`
+ *does not* know anything about dates, create or modify time
+ *does not* do anything with users, groups, or permissions
+ *does not* understand depth
+ *does not* does not understand other file systems, hard drives, or FUSEs and may scan everything
+ *does not* do anything with file sizes (but would be easy to add)
+ *does not* have complex expressions for finding and operating on files
+ *does not* print anything except paths
+ *does not* promp the user for any input

There are a lot of helpful things that `find` can do. We're not aiming to have parity or replicate `find`. It's good and well-known as it is. We will stand apart intentionally with a different API, terms, and with different goals.

## Convenience

### Alias

It doesn't have a shorter command than `search-filenames`. On your system if you want to create an alias you could try `alias sf=search-filenames`.

### zsh function

With `zsh` it's possible to make a function which works like an alias.

`~/dotfiles/zsh/functions/sf`

```sh
#
# search with text
#
search-filenames --root-path "$PWD" \
  --exclude node_modules \
  --exclude .git \
  --exclude dist \
  --exclude public \
  --include "$1"
```

`~/.zshenv` or `~/.zshrc`

```sh
# user functions
fpath=(
  $fpath
  ~/dotfiles/zsh/functions
)
autoload sf
```

## Copying, license, and contributing

Copyright (C) Tony Crowe 2021 <https://tonycrowe.com/contact/>

Thank you for using and contributing to make search-filenames better.

⚠️ Please run `npm run prd` before submitting a patch.

⚖️ search-filenames is free and unlicensed. Anyone can use it, fork, or modify and we the community will try to help whenever possible.
