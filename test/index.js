const path = require("path");
const searchFilenames = require("../src");
const rootPath = path.join(__dirname, "..");

describe("searchFilenames", function () {
  it("can't run without options", function () {
    const includes = ["cli.js", "search-home.js"];
    should.throws(() => searchFilenames());
    should.throws(() =>
      searchFilenames({ rootPath, includes, concurrency: "five" })
    );
    should.throws(() =>
      searchFilenames({ rootPath, includes, concurrency: "-1" })
    );
    should.throws(() =>
      searchFilenames({ rootPath, includes, excludes: "no" })
    );
    should.throws(() =>
      searchFilenames({ rootPath, includes, followSymlinks: "no" })
    );
  });

  it("includes", function (done) {
    const includes = ["cli.js", "search-home.js"];
    const excludes = ["node_modules"];
    const searchOpts = { rootPath, includes, excludes };
    const searcher = searchFilenames(searchOpts);
    const matches = [];
    searcher.on("any", any => matches.push(any));
    searcher.on("end", function () {
      matches.should.have.length(2);
      matches.should.containEql({
        statType: "file",
        absolutePath: path.join(rootPath, "examples", "search-home.js")
      });
      matches.should.containEql({
        statType: "file",
        absolutePath: path.join(rootPath, "src", "cli.js")
      });
      done();
    });
    searcher.start();
  });

  it("includeGlobs", function (done) {
    const includeGlobs = ["*.json"];
    const excludes = ["node_modules"];
    const searchOpts = { rootPath, includeGlobs, excludes };
    const searcher = searchFilenames(searchOpts);
    const matches = [];
    searcher.on("any", any => matches.push(any));
    searcher.on("end", function () {
      matches.should.have.length(2);
      matches.should.containEql({
        statType: "file",
        absolutePath: path.join(rootPath, "package.json")
      });
      matches.should.containEql({
        statType: "file",
        absolutePath: path.join(rootPath, "src", "opts-schema.json")
      });
      done();
    });
    searcher.start();
  });

  it("includeRegExp", function (done) {
    const includeRegExp = ["search-home", ".json$"];
    const excludes = ["node_modules"];
    const searchOpts = { rootPath, includeRegExp, excludes };
    const searcher = searchFilenames(searchOpts);
    const matches = [];
    searcher.on("any", any => matches.push(any));
    searcher.on("end", function () {
      matches.should.have.length(3);
      matches.should.containEql({
        statType: "file",
        absolutePath: path.join(rootPath, "examples", "search-home.js")
      });
      matches.should.containEql({
        statType: "file",
        absolutePath: path.join(rootPath, "package.json")
      });
      matches.should.containEql({
        statType: "file",
        absolutePath: path.join(rootPath, "src", "opts-schema.json")
      });
      done();
    });
    searcher.start();
  });
});
